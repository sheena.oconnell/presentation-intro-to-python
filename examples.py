def print_repeated(s,i=1):
    """print s i times, then return the printed value
    Args:
        s (str): the string
        i (int): how many times should we print i
    Returns:
        str: the printed value
    """
    computed_value = s * i
    print(computed_value)
    return computed_value


class Insect:
    def __init__(self):
        print("hatching insect: ",self)

class Moth(Insect):
    def __repr__(self):
        return "The little moth that could " + str(id(self))

class Bee(Insect):
    all_bees = []
    has_pollen = False
    def __init__(self):
        super(Bee,self).__init__()
        Bee.all_bees.append(self)

    def __repr__(self):
        if self.has_pollen:
            return "<Bee (Full)>"
        else:
            return "<Bee (Empty)>"

    def deposit_pollen(self):
        assert self.has_pollen, "must gather pollen first"
        self.has_pollen = False
        print("Pollen successfully deposited.")

    def gather_pollen(self):
        assert not self.has_pollen, "must deposit pollen first"
        self.has_pollen = True
        print("Pollen successfully gathered.")


